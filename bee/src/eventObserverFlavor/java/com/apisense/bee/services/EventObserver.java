package com.apisense.bee.services;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

import com.apisense.bee.R;
import com.apisense.bee.utils.accessibilitySting.AccessibilityEventWrapper;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import io.apisense.sting.phone.system.WindowChangeDetectingService;

/**
 * Created by Mohammad Naseri
 */

public class EventObserver extends WindowChangeDetectingService {
    private static final String TAG = EventObserver.class.getName();

    private static OnAccessibilityEvent callback;
    private static Stack<StackElement> events = new Stack<>();
    private static StackElement lastEvent = null;
    private static String tempInput = "";
    private static AccessibilityEventWrapper tempWrapper;

    private static final String EDIT_TEXT_CLASS = "android.widget.EditText";
    private static final String WIDGET_CLASS = "android.widget";
    private static final String BEE_ACESSIBILITY_ID = "com.apisense.bee.debug/com.apisense.bee.services.EventObserver";

    @Override
    public void onServiceConnected() {
        Log.d(TAG, "onServiceConnected");

        long t_start = SystemClock.uptimeMillis();
        Log.d(TAG, "EXECUTION START-TIME: " + t_start);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        super.onAccessibilityEvent(event);


        AccessibilityManager am = (AccessibilityManager) getSystemService(ACCESSIBILITY_SERVICE);


        if (events.size() > 0)
            lastEvent = events.lastElement();

        if (event.getEventType() == AccessibilityEvent.TYPE_VIEW_TEXT_SELECTION_CHANGED &&
                (lastEvent.eventType.equals(AccessibilityEvent.eventTypeToString(AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED))
                        || (lastEvent.eventType.equals(AccessibilityEvent.eventTypeToString(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED))
                ))) {
            warnUser(am);
            if (event.getText().size() != 0) {
                if (event.isPassword()) {
                    if (!event.getText().get(0).equals(""))
                        tempInput += getLastCharacter(String.valueOf(event.getText().get(0)));
                    tempWrapper = new AccessibilityEventWrapper(event, tempInput);
                } else {
                    tempInput = String.valueOf(event.getText().get(0));
                    tempWrapper = new AccessibilityEventWrapper(event, tempInput);
                }
            }
        }

        if (event.getEventType() == AccessibilityEvent.TYPE_VIEW_FOCUSED &&
                (lastEvent.eventType.equals(AccessibilityEvent.eventTypeToString(AccessibilityEvent.TYPE_VIEW_TEXT_SELECTION_CHANGED))
                        || (lastEvent.eventType.equals(AccessibilityEvent.eventTypeToString(AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED))))) {

            if (!tempInput.isEmpty()) {
                Log.d(TAG, "onAccessibilityEvent: " + tempWrapper.toString());
                if (callback != null) {
                    saveHashPassword(tempWrapper);
                    callback.sendData(tempWrapper);
                }
            }

            if (!event.isPassword()) {
                if (event.getClassName().equals(EDIT_TEXT_CLASS))
                    if (!event.getText().isEmpty())
                        tempInput = String.valueOf(event.getText().get(0));
                    else
                        tempInput = "";
            } else {
                tempInput = "";
            }

        }

        if (event.getEventType() == AccessibilityEvent.TYPE_VIEW_CLICKED) {
            if (!tempInput.equals("")) {
                Log.d(TAG, "onAccessibilityEvent: " + tempWrapper.toString());
                if (callback != null) {
                    saveHashPassword(tempWrapper);
                    callback.sendData(tempWrapper);

                    AccessibilityEventWrapper accessibilityEventWrapper = new AccessibilityEventWrapper(event, null);
                    Log.d(TAG, "onAccessibilityEvent: " + accessibilityEventWrapper.toString());

                    saveHashPassword(accessibilityEventWrapper);
                    callback.sendData(accessibilityEventWrapper);
                }
                tempInput = "";
            }
        }

        if (event.getEventType() == AccessibilityEvent.TYPE_VIEW_HOVER_ENTER && event.getText() != null) {
            if (event.getText().get(0).equals("delete")) {
                tempInput = removeLastChar(tempInput);
                tempWrapper = new AccessibilityEventWrapper(event, tempInput);
            } else {
                tempInput += String.valueOf(event.getText().get(0));
                tempWrapper = new AccessibilityEventWrapper(event, tempInput);
            }
        }

        if (event.getText().size() != 0 && event.getClassName() != null && tempInput.isEmpty()
                && !event.getClassName().toString().equals(EDIT_TEXT_CLASS)
                && event.getClassName().toString().contains(WIDGET_CLASS)) {
            AccessibilityEventWrapper accessibilityEventWrapper = new AccessibilityEventWrapper(event, null);
            Log.d(TAG, "onAccessibilityEvent: " + accessibilityEventWrapper.toString());
            if (callback != null) {
                saveHashPassword(accessibilityEventWrapper);
                callback.sendData(accessibilityEventWrapper);
            }
            tempInput = "";
        }


        if (event.getEventType() != 0) {
            String packageName = "";
            if (event.getPackageName() != null) {
                packageName = event.getPackageName().toString();
            }
            events.push(new StackElement(AccessibilityEvent.eventTypeToString(event.getEventType()), packageName));
        }

    }

    @Override
    public void onInterrupt() {
        Log.d(TAG, "onInterrupt");
        super.onDestroy();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    public static void createCallback(OnAccessibilityEvent inputCallback) {
        callback = inputCallback;
    }

    public interface OnAccessibilityEvent {
        void sendData(AccessibilityEventWrapper event);
    }

    public String getLastCharacter(String input) {
        return input.substring(input.length() - 1);
    }

    private String removeLastChar(String str) {
        if (str.length() > 0)
            return str.substring(0, str.length() - 1);
        return str;
    }

    static class StackElement {
        StackElement(String eventType, String eventPackage) {
            this.eventType = eventType;
            this.eventPackage = eventPackage;
        }

        public final String eventType;
        public final String eventPackage;
    }

    public void saveHashPassword(AccessibilityEventWrapper wrapper) {
        if (wrapper.isPassword) {
            String password = wrapper.text;
            try {
                MessageDigest digest = MessageDigest.getInstance("MD5");
                byte[] hashedBytes = digest.digest(password.getBytes("UTF-8"));
                String hashedPassword = convertByteArrayToHexString(hashedBytes);

                SharedPreferences sharedPref = getSharedPreferences(getPackageName() + "_preferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                Set<String> packages = sharedPref.getStringSet(hashedPassword, new HashSet<String>());

                packages.add(wrapper.packageName);

                editor.putStringSet(hashedPassword, packages);
                editor.commit();


            } catch (Exception e) {
            }
        }
    }

    private static String convertByteArrayToHexString(byte[] arrayBytes) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayBytes.length; i++) {
            stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return stringBuffer.toString();
    }

    public void warnUser(AccessibilityManager am) {
        final List<AccessibilityServiceInfo> enabledService = new ArrayList<>(new HashSet<>(am.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_ALL_MASK)));
        for (AccessibilityServiceInfo aci : enabledService) {
            if (aci.getId().equals(BEE_ACESSIBILITY_ID)) {
                enabledService.remove(aci);
            }
        }
        if (enabledService.size() > 0) {
            pushNotification();
        }
    }

    public void pushNotification() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher_bee)
                .setContentTitle("Warning")
                .setContentText("Enabled accessibility services can capture your inputs. You can turn them off for now.");

        NotificationManager mNotificationManager =

                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setAutoCancel(true);

        mNotificationManager.notify(001, mBuilder.build());

    }
}
